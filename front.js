$("#etapas").change(function () {
    var etapas = $("#etapas").val();
    etapas = parseInt(etapas);
    aux = ""
    for (var i = 1; i < (etapas + 1); i++) {
        aux += '<tr><th>' + i + '</th><td> <input class="form-control text-center" type="text" value="0" id="etapa_' + i + '" required></td></tr>';
    }
    $("#tablaEtapas").html(aux);
});


$("#decanoCB").change(function () {
    if (this.checked) {
        $("#decano").prop('disabled', false);
    } else {
        $("#decano").prop('disabled', true).val("0.0");
    }
});

$("#undecanoCB").change(function () {
    if (this.checked) {
        $("#undecano").prop('disabled', false);
    } else {
        $("#undecano").prop('disabled', true).val("0.0");
    }
});

$("#dodecanoCB").change(function () {
    if (this.checked) {
        $("#dodecano").prop('disabled', false);
    } else {
        $("#dodecano").prop('disabled', true).val("0.0");
    }
});

$("#metanoCB").change(function () {
    if (this.checked) {
        $("#metano").prop('disabled', false);
    } else {
        $("#metano").prop('disabled', true).val("0.0");
    }
});

$("#etanoCB").change(function () {
    if (this.checked) {
        $("#etano").prop('disabled', false);
    } else {
        $("#etano").prop('disabled', true).val("0.0");
    }
});

$("#propanoCB").change(function () {
    if (this.checked) {
        $("#propano").prop('disabled', false);
    } else {
        $("#propano").prop('disabled', true).val("0.0");
    }
});

$("#butanoCB").change(function () {
    if (this.checked) {
        $("#butano").prop('disabled', false);
    } else {
        $("#butano").prop('disabled', true).val("0.0");
    }
});

$("#pentanoCB").change(function () {
    if (this.checked) {
        $("#pentano").prop('disabled', false);
    } else {
        $("#pentano").prop('disabled', true).val("0.0");
    }
});

$("#hexanoCB").change(function () {
    if (this.checked) {
        $("#hexano").prop('disabled', false);
    } else {
        $("#hexano").prop('disabled', true).val("0.0");
    }
});

$("#heptanoCB").change(function () {
    if (this.checked) {
        $("#heptano").prop('disabled', false);
    } else {
        $("#heptano").prop('disabled', true).val("0.0");
    }
});



$("#metanoGCB").change(function () {
    if (this.checked) {
        $("#metanoG").prop('disabled', false);
    } else {
        $("#metanoG").prop('disabled', true).val("0.0");
    }
});

$("#etanoGCB").change(function () {
    if (this.checked) {
        $("#etanoG").prop('disabled', false);
    } else {
        $("#etanoG").prop('disabled', true).val("0.0");
    }
});

$("#propanoGCB").change(function () {
    if (this.checked) {
        $("#propanoG").prop('disabled', false);
    } else {
        $("#propanoG").prop('disabled', true).val("0.0");
    }
});

$("#butanoGCB").change(function () {
    if (this.checked) {
        $("#butanoG").prop('disabled', false);
    } else {
        $("#butanoG").prop('disabled', true).val("0.0");
    }
});

$("#pentanoGCB").change(function () {
    if (this.checked) {
        $("#pentanoG").prop('disabled', false);
    } else {
        $("#pentanoG").prop('disabled', true).val("0.0");
    }
});

$("#hexanoGCB").change(function () {
    if (this.checked) {
        $("#hexanoG").prop('disabled', false);
    } else {
        $("#hexanoG").prop('disabled', true).val("0.0");
    }
});

$("#heptanoGCB").change(function () {
    if (this.checked) {
        $("#heptanoG").prop('disabled', false);
    } else {
        $("#heptanoG").prop('disabled', true).val("0.0");
    }
});



$("#limpiar").click(function () {
    $("#flujoLiquido").val('0');
    $("#temperaturaLiquido").val('0');
    $("#presionLiquido").val('0');
    $("#decano").val('0.0').prop('disabled', true);
    $("#undecano").val('0.0').prop('disabled', true);
    $("#dodecano").val('0.0').prop('disabled', true);
    $("#flujoGas").val('0');
    $("#temperaturaGas").val('0');
    $("#presionGas").val('0');
    $("#metano").val('0.0').prop('disabled', true);
    $("#etano").val('0.0').prop('disabled', true);
    $("#propano").val('0.0').prop('disabled', true);
    $("#butano").val('0.0').prop('disabled', true);
    $("#pentano").val('0.0').prop('disabled', true);
    $("#hexano").val('0.0').prop('disabled', true);
    $("#heptano").val('0.0').prop('disabled', true);
    $("#metanoG").val('0.0').prop('disabled', true);
    $("#etanoG").val('0.0').prop('disabled', true);
    $("#propanoG").val('0.0').prop('disabled', true);
    $("#butanoG").val('0.0').prop('disabled', true);
    $("#pentanoG").val('0.0').prop('disabled', true);
    $("#hexanoG").val('0.0').prop('disabled', true);
    $("#heptanoG").val('0.0').prop('disabled', true);
    $("#presionTorre").val('0');
    $("#etapas").val('0');
    $('input:checkbox').removeAttr('checked');
    $("#resultados").css("display", "none");
});


function imprimirResultados(componenetesEntrada, etapas, xNormalizados, kij, tj, yNormalizados, vj2, lj2) {
    vj2 = vj2.reverse();
    lj2 = lj2.reverse();
    tj = tj.reverse();
    tituloComposicion = '<tr><th scope="col">Componentes</th>';
    tituloVaporComposicion = '<tr><th scope="col">Componentes</th>';
    tituloKij = '<tr><th scope="col">Componenetes</th>';
    for (i = 0; i < etapas; i++) {
        tituloComposicion += '<th scope="col">' + (i + 1) + '</th>';
        tituloVaporComposicion += '<th scope="col">' + (i + 1) + '</th>';
        tituloKij += '<th scope="col">' + (i + 1) + '</th>';
    }
    tituloComposicion += '</tr>';
    tituloVaporComposicion += '</tr>';
    tituloKij += '</tr>';
    $("#resultadosComposicionTitulo").html(tituloComposicion);
    $("#resultadosComposicionVaporTitulo").html(tituloVaporComposicion);
    $("#resultadosKijTitulo").html(tituloKij);

    bodyComposicion = '';
    bodyVaporComposicion = '';
    auxComposicion = '';
    auxVaporComposicion = '';
    bodyKij = '';
    auxKij = '';
    for (i = 0; i < componenetesEntrada.length; i++) {
        bodyVaporComposicion = '<tr> <th>' + componenetesEntrada[i][0] + '</th>';
        bodyComposicion = '<tr> <th>' + componenetesEntrada[i][0] + '</th>';
        bodyKij = '<tr> <th>' + componenetesEntrada[i][0] + '</th>';
        for (j = 0; j < etapas; j++) {
            bodyComposicion += '<td>' + xNormalizados[i][j] + '</td>';
            bodyVaporComposicion += '<td>' + yNormalizados[i][j] + '</td>';
            bodyKij += '<td>' + kij[i][j] + '</td>';
        }
        bodyComposicion += '</tr>';
        bodyVaporComposicion += '</tr>';
        auxComposicion += bodyComposicion;
        auxVaporComposicion += bodyVaporComposicion;
        bodyKij += '</tr>';
        auxKij += bodyKij;
    }
    $("#resultadosComposicionCuerpo").html(auxComposicion);
    $("#resultadosComposicionVaporCuerpo").html(auxVaporComposicion);
    $("#resultadosKijCuerpo").html(auxKij);

    titulo = '<tr>';
    cuerpo = '<tr>';
    titulovj = '<tr>';
    cuerpovj = '<tr>';
    titulolj = '<tr>';
    cuerpolj = '<tr>';
    for (j = 0; j < etapas; j++) {
        titulo += '<th>' + (j + 1) + '</th>';
        cuerpo += '<td>' + tj[j] + '</td>';
        
        titulovj += '<th>' + (j + 1) + '</th>';
        cuerpovj += '<td>' + vj2[j] + '</td>';
        
        titulolj += '<th>' + (j + 1) + '</th>';
        cuerpolj += '<td>' + lj2[j] + '</td>';
    }
    titulo += '</tr>';
    cuerpo += '</tr>';
    titulovj += '</tr>';
    cuerpovj += '</tr>';
    titulolj += '</tr>';
    cuerpolj += '</tr>';
    $("#resultadosTemperaturaTitulo").html(titulo);
    $("#resultadosTemperaturaCuerpo").html(cuerpo);

    $("#resultadosVjTitulo").html(titulovj);
    $("#resultadosVjCuerpo").html(cuerpovj);

    $("#resultadosLjTitulo").html(titulolj);
    $("#resultadosLjCuerpo").html(cuerpolj);


    $("#resultados").css("display", "flex");

    etapasVector = [];

    for (j = 0; j < etapas; j++) {
        etapasVector.push((j + 1));
    }

    var datosgraficaLiquido = [];
    var datosgraficaVapor = [];
    var datosgraficaKij = [];
    for (i = 0; i < componenetesEntrada.length; i++) {
        datos = [];
        datosVapor = [];
        datosKij = [];
        for (j = 0; j < etapas; j++) {
            datos.push([xNormalizados[i][j]]);
            datosVapor.push([yNormalizados[i][j]]);
            datosKij.push([kij[i][j]]);
        }
        datosgraficaLiquido.push({
            label: componenetesEntrada[i][0],
            lineTension: 0.3,
            backgroundColor: "rgba(0, 0, 0, 0.0)",
            borderColor: componenetesEntrada[i][3],
            pointRadius: 3,
            pointBackgroundColor: componenetesEntrada[i][3],
            pointBorderColor: componenetesEntrada[i][3],
            data: datos
        });

        datosgraficaVapor.push({
            label: componenetesEntrada[i][0],
            lineTension: 0.3,
            backgroundColor: "rgba(0, 0, 0, 0.0)",
            borderColor: componenetesEntrada[i][3],
            pointRadius: 3,
            pointBackgroundColor: componenetesEntrada[i][3],
            pointBorderColor: componenetesEntrada[i][3],
            data: datosVapor
        });
        datosgraficaKij.push({
            label: componenetesEntrada[i][0],
            lineTension: 0.3,
            backgroundColor: "rgba(0, 0, 0, 0.0)",
            borderColor: componenetesEntrada[i][3],
            pointRadius: 3,
            pointBackgroundColor: componenetesEntrada[i][3],
            pointBorderColor: componenetesEntrada[i][3],
            data: datosKij
        });
    }
    Chart.defaults.global.defaultFontFamily = 'Nunito', '-apple-system,system-ui,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,sans-serif';
    Chart.defaults.global.defaultFontColor = '#858796';
    var xNormalizadoChart = document.getElementById("xNormalizadoChart");
    var myLineChart = new Chart(xNormalizadoChart, {
        type: 'line',
        data: {
            labels: etapasVector,
            datasets: datosgraficaLiquido,
        },
        options: {
            maintainAspectRatio: false,
            layout: {
                padding: {
                    left: 10,
                    right: 25,
                    top: 25,
                    bottom: 0
                }
            },
            scales: {
                xAxes: [{
                    time: {
                        unit: 'date'
                    },
                    gridLines: {
                        display: false,
                        drawBorder: false
                    },
                    ticks: {
                        maxTicksLimit: 7
                    }
                }],
                yAxes: [{
                    ticks: {
                        maxTicksLimit: 5,
                        padding: 10,
                    },
                    gridLines: {
                        color: "rgb(234, 236, 244)",
                        zeroLineColor: "rgb(234, 236, 244)",
                        drawBorder: false,
                        borderDash: [2],
                        zeroLineBorderDash: [2]
                    }
                }],
            },
            legend: {
                display: true,
                position: 'top',
                labels: {
                    boxWidth: 10,
                    fontColor: 'black'
                }
            },
            tooltips: {
                backgroundColor: "rgb(255,255,255)",
                bodyFontColor: "#858796",
                titleMarginBottom: 10,
                titleFontColor: '#6e707e',
                titleFontSize: 14,
                borderColor: '#dddfeb',
                borderWidth: 1,
                xPadding: 15,
                yPadding: 15,
                displayColors: false,
                intersect: false,
                mode: 'index',
                caretPadding: 10,

            }
        }
    });


    var yNormalizadoChart = document.getElementById("yNormalizadoChart");
    var myLineChart = new Chart(yNormalizadoChart, {
        type: 'line',
        data: {
            labels: etapasVector,
            datasets: datosgraficaVapor,
        },
        options: {
            maintainAspectRatio: false,
            layout: {
                padding: {
                    left: 10,
                    right: 25,
                    top: 25,
                    bottom: 0
                }
            },
            scales: {
                xAxes: [{
                    time: {
                        unit: 'date'
                    },
                    gridLines: {
                        display: false,
                        drawBorder: false
                    },
                    ticks: {
                        maxTicksLimit: 7
                    }
                }],
                yAxes: [{
                    ticks: {
                        maxTicksLimit: 5,
                        padding: 10,
                    },
                    gridLines: {
                        color: "rgb(234, 236, 244)",
                        zeroLineColor: "rgb(234, 236, 244)",
                        drawBorder: false,
                        borderDash: [2],
                        zeroLineBorderDash: [2]
                    }
                }],
            },
            legend: {
                display: true,
                position: 'top',
                labels: {
                    boxWidth: 10,
                    fontColor: 'black'
                }
            },
            tooltips: {
                backgroundColor: "rgb(255,255,255)",
                bodyFontColor: "#858796",
                titleMarginBottom: 10,
                titleFontColor: '#6e707e',
                titleFontSize: 14,
                borderColor: '#dddfeb',
                borderWidth: 1,
                xPadding: 15,
                yPadding: 15,
                displayColors: false,
                intersect: false,
                mode: 'index',
                caretPadding: 10,

            }
        }
    });

    var temperaturaChart = document.getElementById("temperaturaChart");
    var myLineChart2 = new Chart(temperaturaChart, {
        type: 'line',
        data: {
            labels: etapasVector,
            datasets: [{
                label: "Earnings",
                lineTension: 0.3,
                backgroundColor: "rgba(78, 115, 223, 0.05)",
                borderColor: "rgba(78, 115, 223, 1)",
                pointRadius: 3,
                pointBackgroundColor: "rgba(78, 115, 223, 1)",
                pointBorderColor: "rgba(78, 115, 223, 1)",
                pointHoverRadius: 3,
                pointHoverBackgroundColor: "rgba(78, 115, 223, 1)",
                pointHoverBorderColor: "rgba(78, 115, 223, 1)",
                pointHitRadius: 10,
                pointBorderWidth: 2,
                data: tj,
            }],
        },
        options: {
            maintainAspectRatio: false,
            layout: {
                padding: {
                    left: 10,
                    right: 25,
                    top: 25,
                    bottom: 0
                }
            },
            scales: {
                xAxes: [{
                    time: {
                        unit: 'date'
                    },
                    gridLines: {
                        display: false,
                        drawBorder: false
                    },
                    ticks: {
                        maxTicksLimit: 7
                    }
                }],
                yAxes: [{
                    ticks: {
                        maxTicksLimit: 5,
                        padding: 10,
                    },
                    gridLines: {
                        color: "rgb(234, 236, 244)",
                        zeroLineColor: "rgb(234, 236, 244)",
                        drawBorder: false,
                        borderDash: [2],
                        zeroLineBorderDash: [2]
                    }
                }],
            },
            legend: {
                display: false
            },
            tooltips: {
                backgroundColor: "rgb(255,255,255)",
                bodyFontColor: "#858796",
                titleMarginBottom: 10,
                titleFontColor: '#6e707e',
                titleFontSize: 14,
                borderColor: '#dddfeb',
                borderWidth: 1,
                xPadding: 15,
                yPadding: 15,
                displayColors: false,
                intersect: false,
                mode: 'index',
                caretPadding: 10,

            }
        }
    });


    var temperaturaChart = document.getElementById("LjChart");
    var myLineChart2 = new Chart(temperaturaChart, {
        type: 'line',
        data: {
            labels: etapasVector,
            datasets: [{
                label: "Earnings",
                lineTension: 0.3,
                backgroundColor: "rgba(78, 115, 223, 0.05)",
                borderColor: "rgba(78, 115, 223, 1)",
                pointRadius: 3,
                pointBackgroundColor: "rgba(78, 115, 223, 1)",
                pointBorderColor: "rgba(78, 115, 223, 1)",
                pointHoverRadius: 3,
                pointHoverBackgroundColor: "rgba(78, 115, 223, 1)",
                pointHoverBorderColor: "rgba(78, 115, 223, 1)",
                pointHitRadius: 10,
                pointBorderWidth: 2,
                data: lj2,
            }],
        },
        options: {
            maintainAspectRatio: false,
            layout: {
                padding: {
                    left: 10,
                    right: 25,
                    top: 25,
                    bottom: 0
                }
            },
            scales: {
                xAxes: [{
                    time: {
                        unit: 'date'
                    },
                    gridLines: {
                        display: false,
                        drawBorder: false
                    },
                    ticks: {
                        maxTicksLimit: 7
                    }
                }],
                yAxes: [{
                    ticks: {
                        maxTicksLimit: 5,
                        padding: 10,
                    },
                    gridLines: {
                        color: "rgb(234, 236, 244)",
                        zeroLineColor: "rgb(234, 236, 244)",
                        drawBorder: false,
                        borderDash: [2],
                        zeroLineBorderDash: [2]
                    }
                }],
            },
            legend: {
                display: false
            },
            tooltips: {
                backgroundColor: "rgb(255,255,255)",
                bodyFontColor: "#858796",
                titleMarginBottom: 10,
                titleFontColor: '#6e707e',
                titleFontSize: 14,
                borderColor: '#dddfeb',
                borderWidth: 1,
                xPadding: 15,
                yPadding: 15,
                displayColors: false,
                intersect: false,
                mode: 'index',
                caretPadding: 10,

            }
        }
    });




    var vjChart = document.getElementById("VjChart");
    var myLineChart5 = new Chart(vjChart, {
        type: 'line',
        data: {
            labels: etapasVector,
            datasets: [{
                label: "Earnings",
                lineTension: 0.3,
                backgroundColor: "rgba(78, 115, 223, 0.05)",
                borderColor: "rgba(78, 115, 223, 1)",
                pointRadius: 3,
                pointBackgroundColor: "rgba(78, 115, 223, 1)",
                pointBorderColor: "rgba(78, 115, 223, 1)",
                pointHoverRadius: 3,
                pointHoverBackgroundColor: "rgba(78, 115, 223, 1)",
                pointHoverBorderColor: "rgba(78, 115, 223, 1)",
                pointHitRadius: 10,
                pointBorderWidth: 2,
                data: vj2,
            }],
        },
        options: {
            maintainAspectRatio: false,
            layout: {
                padding: {
                    left: 10,
                    right: 25,
                    top: 25,
                    bottom: 0
                }
            },
            scales: {
                xAxes: [{
                    time: {
                        unit: 'date'
                    },
                    gridLines: {
                        display: false,
                        drawBorder: false
                    },
                    ticks: {
                        maxTicksLimit: 7
                    }
                }],
                yAxes: [{
                    ticks: {
                        maxTicksLimit: 5,
                        padding: 10,
                    },
                    gridLines: {
                        color: "rgb(234, 236, 244)",
                        zeroLineColor: "rgb(234, 236, 244)",
                        drawBorder: false,
                        borderDash: [2],
                        zeroLineBorderDash: [2]
                    }
                }],
            },
            legend: {
                display: false
            },
            tooltips: {
                backgroundColor: "rgb(255,255,255)",
                bodyFontColor: "#858796",
                titleMarginBottom: 10,
                titleFontColor: '#6e707e',
                titleFontSize: 14,
                borderColor: '#dddfeb',
                borderWidth: 1,
                xPadding: 15,
                yPadding: 15,
                displayColors: false,
                intersect: false,
                mode: 'index',
                caretPadding: 10,

            }
        }
    });


    var kChart = document.getElementById("kChart");
    var myLineChart3 = new Chart(kChart, {
        type: 'line',
        data: {
            labels: etapasVector,
            datasets: datosgraficaKij,
        },
        options: {
            maintainAspectRatio: false,
            layout: {
                padding: {
                    left: 10,
                    right: 25,
                    top: 25,
                    bottom: 0
                }
            },
            scales: {
                xAxes: [{
                    time: {
                        unit: 'date'
                    },
                    gridLines: {
                        display: false,
                        drawBorder: false
                    },
                    ticks: {
                        maxTicksLimit: 7
                    }
                }],
                yAxes: [{
                    ticks: {
                        maxTicksLimit: 5,
                        padding: 10,
                    },
                    gridLines: {
                        color: "rgb(234, 236, 244)",
                        zeroLineColor: "rgb(234, 236, 244)",
                        drawBorder: false,
                        borderDash: [2],
                        zeroLineBorderDash: [2]
                    }
                }],
            },
            legend: {
                display: true,
                position: 'top',
                labels: {
                    boxWidth: 10,
                    fontColor: 'black'
                }
            },
            tooltips: {
                backgroundColor: "rgb(255,255,255)",
                bodyFontColor: "#858796",
                titleMarginBottom: 10,
                titleFontColor: '#6e707e',
                titleFontSize: 14,
                borderColor: '#dddfeb',
                borderWidth: 1,
                xPadding: 15,
                yPadding: 15,
                displayColors: false,
                intersect: false,
                mode: 'index',
                caretPadding: 10,

            }
        }
    });
}