/* START Simulación */
function simulacion() {
    var start = Date.now();
    //$('.modal').modal('show');

    /* START Obtiene datos de la interfaz  */

    let flujoLiquido = parseFloat($("#flujoLiquido").val());
    let temperaturaLiquido = parseFloat($("#temperaturaLiquido").val());
    let presionLiquido = parseFloat($("#presionLiquido").val());
    let metano = parseFloat($("#metano").val());
    let etano = parseFloat($("#etano").val());
    let propano = parseFloat($("#propano").val());
    let butano = parseFloat($("#butano").val());
    let pentano = parseFloat($("#pentano").val());
    let hexano = parseFloat($("#hexano").val());
    let heptano = parseFloat($("#heptano").val());
    let decano = parseFloat($("#decano").val());
    let undecano = parseFloat($("#undecano").val());
    let dodecano = parseFloat($("#dodecano").val());
    let flujoGas = parseFloat($("#flujoGas").val());
    let temperaturaGas = parseFloat($("#temperaturaGas").val());
    let presionGas = parseFloat($("#presionGas").val());
    let metanoG = parseFloat($("#metanoG").val());
    let etanoG = parseFloat($("#etanoG").val());
    let propanoG = parseFloat($("#propanoG").val());
    let butanoG = parseFloat($("#butanoG").val());
    let pentanoG = parseFloat($("#pentanoG").val());
    let hexanoG = parseFloat($("#hexanoG").val());
    let heptanoG = parseFloat($("#heptanoG").val());
    let presionTorre = parseFloat($("#presionTorre").val());
    let etapas = parseInt($("#etapas").val());
    /* END Decaración de variables */

    /* STAR Añade datos a matriz */



    var componentesEntrada = []

    if ($("#metanoGCB").is(':checked') == true && $("#metanoCB").is(':checked') == true) {
        componentesEntrada.push(["Metano", metano, metanoG, "#C0392B"]);
    } else if ($("#metanoGCB").is(':checked') == true) {
        componentesEntrada.push(["Metano", 0, metanoG, "#C0392B"]);
    } else if ($("#metanoCB").is(':checked') == true) {
        componentesEntrada.push(["Metano", metano, 0, "#C0392B"]);
    }
    if ($("#etanoGCB").is(':checked') == true && $("#etanoCB").is(':checked') == true) {
        componentesEntrada.push(["Etano", etano, etanoG, "#8E44AD"]);
    } else if ($("#etanoGCB").is(':checked') == true) {
        componentesEntrada.push(["Etano", 0, etanoG, "#8E44AD"]);
    } else if ($("#etanoCB").is(':checked') == true) {
        componentesEntrada.push(["Etano", etano, 0, "#8E44AD"]);
    }
    if ($("#propanoGCB").is(':checked') == true && $("#propanoCB").is(':checked') == true) {
        componentesEntrada.push(["Propano", propano, propanoG, "#3498DB"]);
    } else if ($("#propanoGCB").is(':checked') == true) {
        componentesEntrada.push(["Propano", 0, propanoG, "#3498DB"]);
    } else if ($("#propanoCB").is(':checked') == true) {
        componentesEntrada.push(["Propano", propano, 0, "#3498DB"]);
    }
    if ($("#butanoGCB").is(':checked') == true && $("#butanoCB").is(':checked') == true) {
        componentesEntrada.push(["Butano", butano, butanoG, "#16A085"]);
    } else if ($("#butanoGCB").is(':checked') == true) {
        componentesEntrada.push(["Butano", 0, butanoG, "#16A085"]);
    } else if ($("#butanoCB").is(':checked') == true) {
        componentesEntrada.push(["Butano", butano, 0, "#16A085"]);
    }
    if ($("#pentanoGCB").is(':checked') == true && $("#pentanoCB").is(':checked') == true) {
        componentesEntrada.push(["Pentano", pentano, pentanoG], "#2ECC71");
    } else if ($("#pentanoGCB").is(':checked') == true) {
        componentesEntrada.push(["Pentano", 0, pentanoG, "#2ECC71"]);
    } else if ($("#pentanoCB").is(':checked') == true) {
        componentesEntrada.push(["Pentano", pentano, 0, "#2ECC71"]);
    }
    if ($("#hexanoGCB").is(':checked') == true && $("#hexanoCB").is(':checked') == true) {
        componentesEntrada.push(["Hexano", hexano, hexanoG, "#F39C12"]);
    } else if ($("#hexanoGCB").is(':checked') == true) {
        componentesEntrada.push(["Hexano", 0, hexanoG, "#F39C12"]);
    } else if ($("#hexanoCB").is(':checked') == true) {
        componentesEntrada.push(["Hexano", hexano, 0, "#F39C12"]);
    }
    if ($("#heptanoGCB").is(':checked') == true && $("#heptanoCB").is(':checked') == true) {
        componentesEntrada.push(["Heptano", heptano, heptanoG, "#D35400"]);
    } else if ($("#heptanoGCB").is(':checked') == true) {
        componentesEntrada.push(["Heptano", 0, heptanoG, "#D35400"]);
    } else if ($("#heptanoCB").is(':checked') == true) {
        componentesEntrada.push(["Heptano", heptano, 0, "#D35400"]);
    }

    $("#decanoCB").is(':checked') == true ? componentesEntrada.push(["Decano", decano, 0, "#2C3E50"]) : false;
    $("#undecanoCB").is(':checked') == true ? componentesEntrada.push(["Undecano", undecano, 0, "#7F8C8D"]) : false;
    $("#dodecanoCB").is(':checked') == true ? componentesEntrada.push(["Dodecano", dodecano, 0, "#FFEB3B"]) : false;

    /* START Declaracion de componentes*/
    /* [ nombreComponente, Tc, Pc, A1 , A2 ,A3 A4, A5 ] */
    const componentes = [
        ["Decano", 1111.86, 306, 49.42138, 0.0860271, 0.000002049705, -0.0000000441509, 0.00000000002256651, 6.33557, 6213.998, 317.6512],
        ["Undecano", 1152.0, 282, 54.2521, 0.09427374, 0.000002727295, -0.00000004957262, 0.00000000002544979, 7.21247, 7471.258, 350.7821],
        ["Dodecano", 1188.3, 261.6, 59.0528, 0.1029143, 0.000002243202, -0.00000005378563, 0.0000000000279616, 6.561135, 6739.22, 292.574],
        ["Metano", 343.9, 673.1, 8.245223, 0.03806, 0.000008864745, -0.0000000074661153, 0.000000000001822959, 5.14135, 1742.638, 452.974],
        ["Etano", 550.0, 709.8, 11.51606, 0.001275536, 0.00000854034, -0.00000001106078, 0.000000000003162199, 5.383894, 2847.92, 434.898],
        ["Propano", 665.9, 617.4, 15.58683, 0.02504953, 0.00001404258, -0.00000003526261, 0.0000000001864467, 5.353418, 3371.084, 414.488],
        ["Butano", 765.3, 550.7, 20.79783, 0.03143287, 0.00001928511, -0.00000004588652, 0.00000000002380972, 5.741624, 4126.385, 409.5179],
        ["Pentano", 845.6, 489.5, 25.64627, 0.0389176, 0.00002397294, -0.00000005842615, 0.0000000000307991, 5.853654, 4598.287, 394.4148],
        ["Hexano", 1014.2, 440, 30.17847, 0.05199263, 0.000003048799, -0.000000027639, 0.00000000001346731, 6.039243, 5085.758, 382.794],
        ["Heptano", 972.3, 396.9, 34.96845, 0.0606752, 0.000001213345, -0.0000000293693, 0.00000000001454746, 5.98627, 5278.902, 359.5259]
    ];
    const constanteDeGases = 1.987;
    /* END Declaracion de componentes */



    /* START Añade los componentes de liquido y gas a una sola matriz*/
    var sumaLiquidos = 0;
    var sumaGases = 0;
    for (i = 0; i < componentesEntrada.length; i++) {
        sumaLiquidos += Number(componentesEntrada[i][1].toFixed(3));
    }
    for (i = 0; i < componentesEntrada.length; i++) {
        sumaGases += Number(componentesEntrada[i][2].toFixed(3));
    }
    console.log("Componentes de entrada: ");
    console.log(componentesEntrada);
    /* END Añade los componentes de liquido y gas a una sola matriz*/

    /* Validación de suma sea igual a 1 */
    if (sumaGases == 1 && sumaLiquidos == 1) {
        /* START Calcula tj */
        var tj = [];
        var deltaT = (temperaturaGas - temperaturaLiquido) / (etapas - 1);
        var aux = temperaturaLiquido;
        for (i = 0; i < etapas; i++) {
            tj.push((aux + 459.67));
            aux = temperaturaLiquido + deltaT;
            temperaturaLiquido = aux;
        }
        console.log("Tj inicial: ");
        console.log(JSON.stringify(tj));
        /* END Calcula tj */

        /* START Calcula vj */
        const vj = [];
        for (let i = 0; i < etapas; i++) {
            vj.push(flujoGas);
        }
        console.log("Vj inicial: ");
        console.log(vj);
        /* END Calcula vj */
        /* Busca datos en en la base de datos y los añade */
        const Tc = [];
        const Pc = [];
        const Acx = [];
        for (i = 0; i < componentesEntrada.length; i++) {
            for (j = 0; j < componentes.length; j++) {
                if (componentesEntrada[i][0] == componentes[j][0]) {
                    Tc.push(componentes[j][1]);
                    Pc.push(componentes[j][2]);
                    Acx.push([componentes[j][3], componentes[j][4], componentes[j][5], componentes[j][6], componentes[j][7]]);
                }
            }

        }
        /*START realiza operacion de Antoine */
        /* */
        /* Inicia reglas de mezcla */


        /* Crea el arreglo de acuerdo a las etapas y componentes seleccionados */
        var Aij = createMatrix();
        var Bij = createMatrix();
        /* START Reliza ecuacion Aij y Bij*/
        for (i = 0; i < componentesEntrada.length; i++) {
            for (let j = 0; j < etapas; j++) {
                Aij[i][j] = Math.pow(0.4278 / (Pc[i] * Math.pow((tj[j] / Tc[i]), 2.5)), 0.5);
            }
        }
        for (i = 0; i < componentesEntrada.length; i++) {
            for (var j = 0; j < etapas; j++) {
                Bij[i][j] = 0.0867 / (Pc[i] * ((tj[j] / Tc[i])));
            }

        }
        /* END Realiza ecuacion Aij y Bij */
        /*START realiza operacion de Antoine */
        const constanteDeAntoine = [];
        for (i = 0; i < componentesEntrada.length; i++) {
            for (j = 0; j < componentes.length; j++) {
                if (componentesEntrada[i][0] == componentes[j][0]) {
                    constanteDeAntoine.push([componentes[j][8], componentes[j][9], componentes[j][10]]);
                }
            }
        }
        presionSaturacion = createMatrix();
        for (i = 0; i < componentesEntrada.length; i++) {
            for (j = 0; j < etapas; j++) {
                presionSaturacion[i][j] = Math.exp(constanteDeAntoine[i][0] - (constanteDeAntoine[i][1] / (constanteDeAntoine[i][2] + (tj[j] - 459.67))) + Math.log(Pc[i]));
            }
        }

        kijIdeales = createMatrix();
        for (i = 0; i < componentesEntrada.length; i++) {
            for (j = 0; j < etapas; j++) {
                kijIdeales[i][j] = presionSaturacion[i][j] / presionTorre;
            }
        }
        console.log(kijIdeales);
       


        var x = [];
        var y = createMatrix();
        //do {
            for (i = 0; i < componentesEntrada.length; i++) {
                diagonalA = [];
                diagonalB = [];
                diagonalC = [];
                vectorD = [];

                for (j = 0; j < etapas; j++) {

                    diagonalA[j] = vj[j] + flujoLiquido - vj[0];
                    if (j == 0) { // Primeta etapa
                        vectorD[j] = -(flujoLiquido * componentesEntrada[i][1]);
                        diagonalC[j] = vj[j + 1] * kijIdeales[i][j + 1];
                        diagonalB[j] = -(vj[j + 1] + flujoLiquido - vj[0] + (vj[j] * kijIdeales[i][j]));
                    } else if (j == (etapas - 1)) {  //Ultima Etapa
                        diagonalB[j] = -((flujoLiquido + flujoGas) - vj[0] + (vj[j] * kijIdeales[i][j]));
                        diagonalC[j] = 0 * kijIdeales[i][j];
                        vectorD[j] = -(flujoGas * componentesEntrada[i][2]);
                    } else { //Intermedio 
                        diagonalB[j] = -(vj[j + 1] + flujoLiquido - vj[0] + (vj[j] * kijIdeales[i][j]));
                        diagonalC[j] = vj[j + 1] * kijIdeales[i][j + 1];
                        vectorD[j] = 0;
                    }

                }
                x.push(thomas(diagonalA, diagonalB, diagonalC, vectorD, etapas));
            }

            var xNormalizado = createMatrix();
            var yNormalizado = createMatrix();
            var validadorKijIdeal = createMatrix();
            var verificarKijIdeal = createMatrix();
            var sumaY = [];
            var sumaX = [];
            for (i = 0; i < etapas; i++) {
                sumaXAux = 0;
                for (j = 0; j < componentesEntrada.length; j++) {
                    sumaXAux += x[j][i];
                }
                sumaX.push(sumaXAux);
            }

            for (i = 0; i < componentesEntrada.length; i++) {
                for (j = 0; j < etapas; j++) {
                    xNormalizado[i][j] = (x[i][j] / sumaX[j]);
                }

            }
            for (i = 0; i < componentesEntrada.length; i++) {
                for (j = 0; j < etapas; j++) {
                    y[i][j] = kijIdeales[i][j] * xNormalizado[i][j];
                }
            }
            //Normalizacion de Y
            for (i = 0; i < etapas; i++) {
                suma = 0;
                for (j = 0; j < componentesEntrada.length; j++) {
                    suma += y[j][i];
                }
                sumaY.push(suma);
            }
            for (i = 0; i < componentesEntrada.length; i++) {
                for (j = 0; j < etapas; j++) {
                    yNormalizado[i][j] = (y[i][j] / sumaY[j]);
                }

            }
            for (i = 0; i < componentesEntrada.length; i++) {
                for (j = 0; j < etapas; j++) {
                    verificarKijIdeal[i][j] = yNormalizado[i][j] / xNormalizado[i][j];
                }
            }
            for (i = 0; i < componentesEntrada.length; i++) {
                for (j = 0; j < etapas; j++) {
                    validadorKijIdeal[i][j] = kijIdeales[i][j] - verificarKijIdeal[i][j];
                }
            }
            console.log((verificarKijIdeal));
            console.log(validadorKijIdeal);
        //} while ();




        console.log("Valores iniciales de x & y: ");
        console.log(x);
        console.log(JSON.stringify(y));
        /* END Inicializa variables x & y */
        matrizMultiplicacionAx = createMatrix();
        matrizMultiplicacionAy = createMatrix();
        matrizMultiplicacionBx = createMatrix();
        matrizMultiplicacionBx = createMatrix();
        matrizMultiplicacionBy = createMatrix();

        for (i = 0; i < componentesEntrada.length; i++) {
            for (j = 0; j < etapas; j++) {
                matrizMultiplicacionAx[i][j] = Aij[i][j] * x[i][j];
                matrizMultiplicacionAy[i][j] = Aij[i][j] * y[i][j];
                matrizMultiplicacionBx[i][j] = Bij[i][j] * x[i][j];
                matrizMultiplicacionBy[i][j] = Bij[i][j] * y[i][j];
            }
        }
        Ax = [];
        Ay = [];
        Bx = [];
        By = [];

        for (i = 0; i < etapas; i++) {
            auxAx = 0;
            auxAy = 0;
            auxBx = 0;
            auxBy = 0;
            for (j = 0; j < componentesEntrada.length; j++) {
                auxAx += matrizMultiplicacionAx[j][i];
                auxAy += matrizMultiplicacionAy[j][i];
                auxBx += matrizMultiplicacionBx[j][i];
                auxBy += matrizMultiplicacionBy[j][i];
            }
            Ax.push(auxAx);
            Ay.push(auxAy);
            Bx.push(auxBx);
            By.push(auxBy);
        }
        Axy = [];
        Bxy = [];
        for (j = 0; j < etapas; j++) {
            Axy.push(Ax[j] * Ay[j]);
            Bxy.push(Bx[j] * By[j]);
        }
        Aj = [];
        Bj = [];
        for (j = 0; j < etapas; j++) {
            Aj.push(Math.sqrt(Axy[j]));
            Bj.push(Math.sqrt(Bxy[j]));
        }

        /* START Entapia Alimentacion  */
        
        var hPuraAlimentacion = [];
        var hijlAlimentacion = [];
        var hijvAlimentacion = [];
        /* START Para alimentacion liquido y gas */
        for (i = 0; i < componentesEntrada.length; i++) {
            sumahpuroAlimentacion = Acx[i][0] * Math.pow(tj[0] - 459.67, 1) / 1 + Acx[i][1] * Math.pow(tj[0] - 459.67, 2) / 2 + Acx[i][2] * Math.pow(tj[0] - 459.67, 3) / 3 + Acx[i][3] * Math.pow(tj[0] - 459.67, 4) / 4 + Acx[i][4] * Math.pow(tj[0] - 459.67, 5) / 5;
            hPuraAlimentacion.push(sumahpuroAlimentacion);
        }
        /* END sumaHpuro Alimentacion */
        /*  Alimentacion */
        for (i = 0; i < componentesEntrada.length; i++) {
            hijlAlimentacion.push(componentesEntrada[i][1] * hPuraAlimentacion[i]);
            hijvAlimentacion.push(componentesEntrada[i][2] * hPuraAlimentacion[i]);
        }
        var hijlAlimentacionSuma = 0;
        var hijvAlimentacionSuma = 0;

        for (i = 0; i < componentesEntrada.length; i++) {
            hijlAlimentacionSuma += hijlAlimentacion[i];
            hijvAlimentacionSuma += hijvAlimentacion[i];
        }

        // var entalpiahFl = hijlAlimentacionSuma + (constanteDeGases * tj[0]) * ((ZjlEntalpia[0] - 1 - (3 * Math.pow(Ax[0], 2) / (2 * Bx[0])) * Math.log(1 + (Bx[0] * presionLiquido) / ZjlEntalpia[0])));
        // var entalpiahFv = hijvAlimentacionSuma + (constanteDeGases * tj[etapas - 1]) * ((ZjvEntalpia[0] - 1 - (3 * Math.pow(Ay[0], 2) / (2 * By[0])) * Math.log(1 + (By[0] * presionGas) / ZjvEntalpia[0])));

        var entalpiahFl = hijlAlimentacionSuma;
        var entalpiahFv = hijvAlimentacionSuma;

        console.log("Entalpia de alimentacion Flujo: ");
        console.log(entalpiahFl);
        console.log("Entalpia de alimentacion Vapor: ");
        console.log(entalpiahFv);
        /* END Para alimentacion */


        /* END Alimentacion  */

        /* END obtiene las Zj liquido y gas  para entalpia  */
        /* END Ecuación de tercer grado */

        /* START Ecuación de tercer grado */
        let Zj = [];
        for (let r = 0; r < etapas; r++) {
            let a = 1;
            let b = -1;
            let c = (Bj[r] * presionTorre) * ((Math.pow(Aj[r], 2) / Bj[r]) - (Bj[r] * presionTorre) - 1);
            let d = -(Math.pow(Aj[r], 2) / Bj[r]) * Math.pow(Bj[r] * presionTorre, 2);
            let xE = -1000;
            let i = 0;
            while (Math.abs(a * 1 * (xE - 0.1) * (xE - 0.1) * (xE - 0.1) + b * 1 * (xE - 0.1) * (xE - 0.1) + c * 1 * (xE - 0.1) + d * 1) > 0.0000001 && xE < 1000) {
                for (i = 0; i <= 20; i++) {
                    var xnew = xE - (a * 1 * Math.pow(xE, 3) + b * 1 * Math.pow(xE, 2) + c * 1 * xE * 1 + d * 1) / (3 * a * Math.pow(xE, 2) + 2 * b * xE * 1 + c * 1);
                    xE = xnew;
                }
                xE = xE + 0.1;
            }

            if (Math.abs(a * 1 * (xE - 0.1) * (xE - 0.1) * (xE - 0.1) + b * 1 * (xE - 0.1) * (xE - 0.1) + c * 1 * (xE - 0.1) + d * 1) < 0.0000001) {
                xE = xE - 0.1;

                var x2 = (-a * xE - b + Math.sqrt(b * b - 3 * a * a * xE * xE - 2 * a * b * xE - 4 * a * c)) / (2 * a);

                var x3 = (-a * xE - b - Math.sqrt(b * b - 3 * a * a * xE * xE - 2 * a * b * xE - 4 * a * c)) / (2 * a);


                Zj.push([xE, x2, x3]);
            }
            else {
                toastr.error('Error al resolver ecuación.', 'Error.');
            }

        }
        /*START Ordena resultados de ecuacion cuadratica */
        for (i = 0; i < etapas; i++) {
            Zj[i] = Zj[i].sort();
        }
        Zv = [];
        Zl = [];
        for (i = 0; i < etapas; i++) {
            Zv[i] = Zj[i][2];
            Zl[i] = Zj[i][1];
        }
        /*END Ordena resultados de ecuacion cuadratica */

        let fiLj = createMatrix();
        let fiVj = createMatrix();
        var kij = createMatrix();

        /* START Constante de equilibrio */
        for (i = 0; i < componentesEntrada.length; i++) {
            for (j = 0; j < etapas; j++) {
                fiVj[i][j] = Math.exp(((Zv[j] - 1) * (Bij[i][j] / Bj[j])) - (Math.log(Zv[j] - (Bj[j] * presionTorre))) - (((Math.pow(Aj[j], 2) / Bj[j]) * (((2 * Aij[i][j]) / Aj[j]) - (Bij[i][j] / Bj[j]))) * (Math.log(1 + (Bj[j] * presionTorre / Zv[j])))));
                fiLj[i][j] = Math.exp(((Zl[j] - 1) * (Bij[i][j] / Bj[j])) - (Math.log(Zl[j] - (Bj[j] * presionTorre))) - (((Math.pow(Aj[j], 2) / Bj[j]) * (((2 * Aij[i][j]) / Aj[j]) - (Bij[i][j] / Bj[j]))) * (Math.log(1 + (Bj[j] * presionTorre / Zl[j])))));
                kij[i][j] = fiLj[i][j] / fiVj[i][j];
            }
        }
        /* END Constante de equilibrio */
        /*Reinicia valores de x */
        x = [];
        /* Reliza thomas para liquidos */

        for (i = 0; i < componentesEntrada.length; i++) {
            diagonalA = [];
            diagonalB = [];
            diagonalC = [];
            vectorD = [];

            for (j = 0; j < etapas; j++) {

                diagonalA[j] = vj[j] + flujoLiquido - vj[0];
                if (j == 0) { // Primeta etapa
                    vectorD[j] = -(flujoLiquido * componentesEntrada[i][1]);
                    diagonalC[j] = vj[j + 1] * kij[i][j + 1];
                    diagonalB[j] = -(vj[j + 1] + flujoLiquido - vj[0] + (vj[j] * kij[i][j]));
                } else if (j == (etapas - 1)) {  //Ultima Etapa
                    diagonalB[j] = -((flujoLiquido + flujoGas) - vj[0] + (vj[j] * kij[i][j]));
                    diagonalC[j] = 0 * kij[i][j];
                    vectorD[j] = -(flujoGas * componentesEntrada[i][2]);
                } else { //Intermedio 
                    diagonalB[j] = -(vj[j + 1] + flujoLiquido - vj[0] + (vj[j] * kij[i][j]));
                    diagonalC[j] = vj[j + 1] * kij[i][j + 1];
                    vectorD[j] = 0;
                }

            }
            x.push(thomas(diagonalA, diagonalB, diagonalC, vectorD, etapas));
        }
        /* START Calcula lj2 */
        var lj = [];
        var lj2 = [];
        for (j = 0; j < etapas; j++) {
            if (j == (etapas - 1)) {
                lj.push((flujoLiquido + flujoGas) - vj[0]);
            } else {
                lj.push(vj[j + 1] + flujoLiquido - vj[0]);
            }

        }
        /* END Calcula lj2 */
        /* START Reliza suma de X y multiplicación por lj2 */
        sumaX = [];
        for (i = 0; i < etapas; i++) {
            suma = 0;
            for (j = 0; j < componentesEntrada.length; j++) {
                suma += x[j][i];
            }
            sumaX.push(suma);
        }
        for (i = 0; i < etapas; i++) {
            lj2.push(lj[i] * sumaX[i]);
        }
        /* END Realiza suma de X y multiplicacion por lj2 */
        /* START Calcula vj2 */
        var vj2 = [];
        for (j = 0; j < etapas; j++) {
            if (j == 0) {
                vj2.push(-lj2[etapas - 1] + flujoLiquido + flujoGas);
            } else {

                vj2.push(lj2[j - 1] - lj2[etapas - 1] + flujoGas);
            }

        }
        /* END Calcula vj2 */

        /* START Nomalizarcion de x y y */
        var xNormalizado = createMatrix();
        var yNormalizado = createMatrix();

        var sumaY = [];
        var sumaX = [];
        for (i = 0; i < etapas; i++) {
            sumaXAux = 0;
            for (j = 0; j < componentesEntrada.length; j++) {
                sumaXAux += x[j][i];
            }
            sumaX.push(sumaXAux);
        }

        for (i = 0; i < componentesEntrada.length; i++) {
            for (j = 0; j < etapas; j++) {
                xNormalizado[i][j] = (x[i][j] / sumaX[j]);
            }

        }
        for (i = 0; i < componentesEntrada.length; i++) {
            for (j = 0; j < etapas; j++) {
                y[i][j] = kij[i][j] * xNormalizado[i][j];
            }
        }
        //Normalizacion de Y
        for (i = 0; i < etapas; i++) {
            suma = 0;
            for (j = 0; j < componentesEntrada.length; j++) {
                suma += y[j][i];
            }
            sumaY.push(suma);
        }
        for (i = 0; i < componentesEntrada.length; i++) {
            for (j = 0; j < etapas; j++) {
                yNormalizado[i][j] = (y[i][j] / sumaY[j]);
            }

        }
        /* END Normalizacion de x & y */
        console.log("xNormalizado");
        console.log(JSON.stringify(xNormalizado));
        console.log("yNormalizado");
        console.log(JSON.stringify(yNormalizado));

        /* START Obtiene los valores de qj de la interfaz */
        qj = [];
        for (j = 0; j < etapas; j++) {
            qj.push(parseFloat($("#etapa_" + (j + 1)).val()));
        }
        /* END Obtiene los valores de qj de la interfaz */

        var tj2 = tj;
        var auxiliarContador = 1;

        /* Inicia bucle */
        do {

            if (auxiliarContador > 1) {
                console.warn("Inicia ciclo: " + auxiliarContador);
                var Aij2 = createMatrix();
                var Bij2 = createMatrix();
                //Reliza ecuacion ai,j

                for (i = 0; i < componentesEntrada.length; i++) {
                    for (j = 0; j < etapas; j++) {
                        Aij2[i][j] = Math.pow(0.4278 / (Pc[i] * Math.pow((tj2[j] / Tc[i]), 2.5)), 0.5);

                    }
                }

                //Realiza ecuacion Bij
                for (i = 0; i < componentesEntrada.length; i++) {
                    for (j = 0; j < etapas; j++) {
                        Bij2[i][j] = 0.0867 / (Pc[i] * ((tj2[j] / Tc[i])));
                    }

                }
                for (i = 0; i < componentesEntrada.length; i++) {
                    for (j = 0; j < etapas; j++) {
                        matrizMultiplicacionAx[i][j] = Aij2[i][j] * xNormalizado[i][j];
                        matrizMultiplicacionAy[i][j] = Aij2[i][j] * yNormalizado[i][j];
                        matrizMultiplicacionBx[i][j] = Bij2[i][j] * xNormalizado[i][j];
                        matrizMultiplicacionBy[i][j] = Bij2[i][j] * yNormalizado[i][j];
                    }
                }
                Ax = [];
                Ay = [];
                Bx = [];
                By = [];
                for (i = 0; i < etapas; i++) {
                    auxAx = 0;
                    auxAy = 0;
                    auxBx = 0;
                    auxBy = 0;
                    for (j = 0; j < componentesEntrada.length; j++) {
                        auxAx = matrizMultiplicacionAx[j][i] + auxAx;
                        auxAy = matrizMultiplicacionAy[j][i] + auxAy;
                        auxBx = matrizMultiplicacionBx[j][i] + auxBx;
                        auxBy = matrizMultiplicacionBy[j][i] + auxBy;
                    }
                    Ax.push(auxAx);
                    Ay.push(auxAy);
                    Bx.push(auxBx);
                    By.push(auxBy);
                }
                Axy = [];
                Bxy = [];
                for (var i = 0; i < Ax.length; i++) {
                    Axy.push(Ax[i] * Ay[i]);
                    Bxy.push(Bx[i] * By[i]);
                }
                Aj2 = [];
                Bj2 = [];
                for (var i = 0; i < Axy.length; i++) {
                    Aj2.push(Math.sqrt(Axy[i]));
                    Bj2.push(Math.sqrt(Bxy[i]));
                }
                let Zj2 = [];

                for (r = 0; r < etapas; r++) {
                    let a = 1;
                    let b = -1;
                    let c = (Bj2[r] * presionTorre) * ((Math.pow(Aj2[r], 2) / Bj2[r]) - (Bj2[r] * presionTorre) - 1);
                    let d = -(Math.pow(Aj2[r], 2) / Bj2[r]) * Math.pow(Bj2[r] * presionTorre, 2);
                    let xEcuacion = -1000;
                    let i = 0;
                    while (Math.abs(a * 1 * (xEcuacion - 0.1) * (xEcuacion - 0.1) * (xEcuacion - 0.1) + b * 1 * (xEcuacion - 0.1) * (xEcuacion - 0.1) + c * 1 * (xEcuacion - 0.1) + d * 1) > 0.0000001 && xEcuacion < 1000) {

                        for (i = 0; i <= 20; i++) {
                            var xnew = xEcuacion - (a * 1 * Math.pow(xEcuacion, 3) + b * 1 * Math.pow(xEcuacion, 2) + c * 1 * xEcuacion * 1 + d * 1) / (3 * a * Math.pow(xEcuacion, 2) + 2 * b * xEcuacion * 1 + c * 1);

                            xEcuacion = xnew;
                        }
                        xEcuacion = xEcuacion + 0.1;
                    }
                    //end of the while loop

                    if (Math.abs(a * 1 * (xEcuacion - 0.1) * (xEcuacion - 0.1) * (xEcuacion - 0.1) + b * 1 * (xEcuacion - 0.1) * (xEcuacion - 0.1) + c * 1 * (xEcuacion - 0.1) + d * 1) < 0.0000001) {
                        xEcuacion = xEcuacion - 0.1;

                        //the formula for the values of x2 and x3

                        var x2 = (-a * xEcuacion - b + Math.sqrt(b * b - 3 * a * a * xEcuacion * xEcuacion - 2 * a * b * xEcuacion - 4 * a * c)) / (2 * a);

                        var x3 = (-a * xEcuacion - b - Math.sqrt(b * b - 3 * a * a * xEcuacion * xEcuacion - 2 * a * b * xEcuacion - 4 * a * c)) / (2 * a);


                        Zj2.push([xEcuacion, x2, x3]);
                    }
                    else {
                        toastr.error("Error al resolver la ecuación. Contador: " + auxiliarContador, "Error");
                    }

                }
                console.log("Contador " + auxiliarContador + " Resultados Zj2: ");
                console.log(Zj2);
                for (i = 0; i < etapas; i++) {
                    Zj2[i] = Zj2[i].sort();
                }
                let Zv2 = [];
                let Zl2 = [];
                for (i = 0; i < etapas; i++) {
                    Zv2[i] = Zj2[i][2];
                    Zl2[i] = Zj2[i][1];
                    if (isNaN(Zv2[i]) == true || isNaN(Zl2[i]) == true) {
                        toastr.error("Error al resolver la ecuación. Contador: " + auxiliarContador, "Error");
                    }
                }


                let fiLj = createMatrix();
                let fiVj = createMatrix();
                var kij = createMatrix();


                for (i = 0; i < componentesEntrada.length; i++) {
                    for (j = 0; j < etapas; j++) {

                        fiVj[i][j] = Math.exp(((Zv2[j] - 1) * (Bij2[i][j] / Bj2[j])) - (Math.log(Zv2[j] - (Bj2[j] * presionTorre))) - (((Math.pow(Aj2[j], 2) / Bj2[j]) * (((2 * Aij2[i][j]) / Aj2[j]) - (Bij2[i][j] / Bj2[j]))) * (Math.log(1 + (Bj2[j] * presionTorre / Zv2[j])))));

                        fiLj[i][j] = Math.exp(((Zl2[j] - 1) * (Bij2[i][j] / Bj2[j])) - (Math.log(Zl2[j] - (Bj2[j] * presionTorre))) - (((Math.pow(Aj2[j], 2) / Bj2[j]) * (((2 * Aij2[i][j]) / Aj2[j]) - (Bij2[i][j] / Bj2[j]))) * (Math.log(1 + (Bj2[j] * presionTorre / Zl2[j])))));
                        //Constante de equilibrio
                        kij[i][j] = fiLj[i][j] / fiVj[i][j];

                    }
                }
                console.log("kij: ");
                console.log(JSON.stringify(kij));
                x = [];

                for (i = 0; i < componentesEntrada.length; i++) {
                    diagonalA = [];
                    diagonalB = [];
                    diagonalC = [];
                    vectorD = [];

                    for (j = 0; j < etapas; j++) {

                        diagonalA[j] = vj[j] + flujoLiquido - vj[0];
                        if (j == 0) { // Primeta etapa
                            vectorD[j] = -(flujoLiquido * componentesEntrada[i][1]);
                            diagonalC[j] = vj[j + 1] * kij[i][j + 1];
                            diagonalB[j] = -(vj[j + 1] + flujoLiquido - vj[0] + (vj[j] * kij[i][j]));
                        } else if (j == (etapas - 1)) {  //Ultima Etapa
                            diagonalB[j] = -((flujoLiquido + flujoGas) - vj[0] + (vj[j] * kij[i][j]));
                            diagonalC[j] = 0 * kij[i][j];
                            vectorD[j] = -(flujoGas * componentesEntrada[i][2]);
                        } else { //Intermedio 
                            diagonalB[j] = -(vj[j + 1] + flujoLiquido - vj[0] + (vj[j] * kij[i][j]));
                            diagonalC[j] = vj[j + 1] * kij[i][j + 1];
                            vectorD[j] = 0;
                        }

                    }
                    x.push(thomas(diagonalA, diagonalB, diagonalC, vectorD, etapas));
                }

                var lj = [];
                var lj2 = [];
                for (j = 0; j < etapas; j++) {
                    if (j == (etapas - 1)) {
                        lj.push((flujoLiquido + flujoGas) - vj[0]);
                    } else {
                        lj.push(vj[j + 1] + flujoLiquido - vj[0]);
                    }

                }
                /* Reliza la suma de componentes seleccionados */
                sumaX = [];
                for (i = 0; i < etapas; i++) {
                    suma = 0;
                    for (j = 0; j < componentesEntrada.length; j++) {
                        suma += x[j][i];
                    }
                    sumaX.push(suma);

                }
                for (i = 0; i < etapas; i++) {
                    lj2.push(lj[i] * sumaX[i]);
                }

                /* calcula vj2 */
                vj2 = [];
                for (j = 0; j < etapas; j++) {
                    if (j == 0) {
                        vj2.push(- lj2[etapas - 1] + flujoLiquido + flujoGas);
                    } else {

                        vj2.push(lj2[j - 1] - lj2[etapas - 1] + flujoGas);
                    }

                }
                /* Nomalizarcion de x y y */
                var xNormalizado = createMatrix();
                var yNormalizado = createMatrix();

                var sumaY = [];
                var sumaX = [];
                for (i = 0; i < etapas; i++) {
                    sumaXAux = 0;
                    for (j = 0; j < componentesEntrada.length; j++) {
                        sumaXAux += x[j][i];
                    }
                    sumaX.push(sumaXAux);
                }

                for (i = 0; i < componentesEntrada.length; i++) {
                    for (j = 0; j < etapas; j++) {
                        xNormalizado[i][j] = (x[i][j] / sumaX[j]);
                    }

                }
                for (i = 0; i < componentesEntrada.length; i++) {
                    for (j = 0; j < etapas; j++) {
                        y[i][j] = kij[i][j] * xNormalizado[i][j];
                    }
                }
                //Normalizacion de Y
                for (i = 0; i < etapas; i++) {
                    suma = 0;
                    for (j = 0; j < componentesEntrada.length; j++) {
                        suma += y[j][i];
                    }
                    sumaY.push(suma);
                }
                for (i = 0; i < componentesEntrada.length; i++) {
                    for (j = 0; j < etapas; j++) {
                        yNormalizado[i][j] = (y[i][j] / sumaY[j]);
                    }

                }
            } else {
                console.warn("Inicia ciclo: " + auxiliarContador);
                var Aij2 = createMatrix();
                var Bij2 = createMatrix();

                /* START Calcula Aij2 y Bij2*/
                for (i = 0; i < componentesEntrada.length; i++) {
                    for (j = 0; j < etapas; j++) {
                        Aij2[i][j] = Math.pow(0.4278 / (Pc[i] * Math.pow((tj2[j] / Tc[i]), 2.5)), 0.5);
                    }
                }
                for (i = 0; i < componentesEntrada.length; i++) {
                    for (j = 0; j < etapas; j++) {
                        Bij2[i][j] = 0.0867 / (Pc[i] * ((tj2[j] / Tc[i])));
                    }

                }
                /* END Calcula Aij2 y Bij2*/

                for (i = 0; i < componentesEntrada.length; i++) {
                    for (j = 0; j < etapas; j++) {
                        matrizMultiplicacionAx[i][j] = Aij2[i][j] * xNormalizado[i][j];
                        matrizMultiplicacionAy[i][j] = Aij2[i][j] * yNormalizado[i][j];
                        matrizMultiplicacionBx[i][j] = Bij2[i][j] * xNormalizado[i][j];
                        matrizMultiplicacionBy[i][j] = Bij2[i][j] * yNormalizado[i][j];
                    }
                }
                Ax = [];
                Ay = [];
                Bx = [];
                By = [];
                for (i = 0; i < etapas; i++) {
                    auxAx = 0;
                    auxAy = 0;
                    auxBx = 0;
                    auxBy = 0;
                    for (j = 0; j < componentesEntrada.length; j++) {
                        auxAx = matrizMultiplicacionAx[j][i] + auxAx;
                        auxAy = matrizMultiplicacionAy[j][i] + auxAy;
                        auxBx = matrizMultiplicacionBx[j][i] + auxBx;
                        auxBy = matrizMultiplicacionBy[j][i] + auxBy;
                    }
                    Ax.push(auxAx);
                    Ay.push(auxAy);
                    Bx.push(auxBx);
                    By.push(auxBy);
                }
                Axy = [];
                Bxy = [];
                for (var i = 0; i < Ax.length; i++) {
                    Axy.push(Ax[i] * Ay[i]);
                    Bxy.push(Bx[i] * By[i]);
                }
                Aj2 = [];
                Bj2 = [];
                for (var i = 0; i < Axy.length; i++) {
                    Aj2.push(Math.sqrt(Axy[i]));
                    Bj2.push(Math.sqrt(Bxy[i]));
                }
                let Zj2 = [];

                for (r = 0; r < etapas; r++) {
                    let a = 1;
                    let b = -1;
                    let c = (Bj2[r] * presionTorre) * ((Math.pow(Aj2[r], 2) / Bj2[r]) - (Bj2[r] * presionTorre) - 1);
                    let d = -(Math.pow(Aj2[r], 2) / Bj2[r]) * Math.pow(Bj2[r] * presionTorre, 2);
                    let xEcuacion = -1000;
                    let i = 0;
                    while (Math.abs(a * 1 * (xEcuacion - 0.1) * (xEcuacion - 0.1) * (xEcuacion - 0.1) + b * 1 * (xEcuacion - 0.1) * (xEcuacion - 0.1) + c * 1 * (xEcuacion - 0.1) + d * 1) > 0.0000001 && xEcuacion < 1000) {

                        for (i = 0; i <= 20; i++) {
                            var xnew = xEcuacion - (a * 1 * Math.pow(xEcuacion, 3) + b * 1 * Math.pow(xEcuacion, 2) + c * 1 * xEcuacion * 1 + d * 1) / (3 * a * Math.pow(xEcuacion, 2) + 2 * b * xEcuacion * 1 + c * 1);

                            xEcuacion = xnew;
                        }
                        xEcuacion = xEcuacion + 0.1;
                    }
                    if (Math.abs(a * 1 * (xEcuacion - 0.1) * (xEcuacion - 0.1) * (xEcuacion - 0.1) + b * 1 * (xEcuacion - 0.1) * (xEcuacion - 0.1) + c * 1 * (xEcuacion - 0.1) + d * 1) < 0.0000001) {
                        xEcuacion = xEcuacion - 0.1;

                        var x2 = (-a * xEcuacion - b + Math.sqrt(b * b - 3 * a * a * xEcuacion * xEcuacion - 2 * a * b * xEcuacion - 4 * a * c)) / (2 * a);

                        var x3 = (-a * xEcuacion - b - Math.sqrt(b * b - 3 * a * a * xEcuacion * xEcuacion - 2 * a * b * xEcuacion - 4 * a * c)) / (2 * a);


                        Zj2.push([xEcuacion, x2, x3]);
                    }
                    else {
                        console.log("error en la ecuacion");
                    }

                }

                for (i = 0; i < etapas; i++) {
                    Zj2[i] = Zj2[i].sort();
                }
                var Zv2 = [];
                var Zl2 = [];
                for (i = 0; i < etapas; i++) {
                    Zv2[i] = Zj2[i][2];
                    Zl2[i] = Zj2[i][1];
                }
            } // Termina ELSE de primer corrida de bucle

            /*Entapia fase de vapor y liquida */
            let hPura = createMatrix();
            let hijv = createMatrix();
            let hijl = createMatrix();


            for (i = 0; i < componentesEntrada.length; i++) {
                for (j = 0; j < etapas; j++) {
                    sumahpuro = 0;
                    sumahpuro = Acx[i][0] * Math.pow(tj2[j] - 459.67, 1) / 1 + Acx[i][1] * Math.pow(tj2[j] - 459.67, 2) / 2 + Acx[i][2] * Math.pow(tj2[j] - 459.67, 3) / 3 + Acx[i][3] * Math.pow(tj2[j] - 459.67, 4) / 4 + Acx[i][4] * Math.pow(tj2[j] - 459.67, 5) / 5;
                    hPura[i][j] = sumahpuro;
                }
            }
            for (i = 0; i < componentesEntrada.length; i++) {
                for (j = 0; j < etapas; j++) {
                    hijv[i][j] = hPura[i][j] * yNormalizado[i][j];
                    hijl[i][j] = hPura[i][j] * xNormalizado[i][j];
                }

            }

            hijvSuma = [];
            hijlSuma = [];

            for (i = 0; i < etapas; i++) {
                suma = 0;
                sumaL = 0;
                for (j = 0; j < componentesEntrada.length; j++) {
                    suma += hijv[j][i];
                    sumaL += hijl[j][i];
                }
                hijvSuma.push(suma);
                hijlSuma.push(sumaL);
            }
            hjv = [];
            hjl = [];
            for (j = 0; j < etapas; j++) {
                entalpiaVapor = hijvSuma[j] + (constanteDeGases * tj2[j]) * ((Zv2[j] - 1 - (3 * Math.pow(Aj2[j], 2) / (2 * Bj2[j])) * Math.log(1 + (Bj2[j] * presionTorre) / Zv2[j])));
                entalpiaLiquida = hijlSuma[j] + (constanteDeGases * tj2[j]) * ((Zl2[j] - 1 - (3 * Math.pow(Aj2[j], 2) / (2 * Bj2[j])) * Math.log(1 + (Bj2[j] * presionTorre) / Zl2[j])));


                hjv.push(entalpiaVapor);
                hjl.push(entalpiaLiquida);

            }
            /* START  integral parcial  */
            /* calculo para el primer termino */
            let primerTerminoPreSuma = createMatrix();
            for (i = 0; i < componentesEntrada.length; i++) {
                for (j = 0; j < etapas; j++) {
                    primerTerminoPreSuma[i][j] = Acx[i][0] * Math.pow(tj2[j] - 459.67, 0) + Acx[i][1] * Math.pow(tj2[j] - 459.67, 1) + Acx[i][2] * Math.pow(tj2[j] - 459.67, 2) + Acx[i][3] * Math.pow(tj2[j] - 459.67, 3) + Acx[i][4] * Math.pow(tj2[j] - 459.67, 4);
                }
            }
            let primerTerminoSuma = createMatrix();
            let primerTerminoSumaLiquido = createMatrix();
            for (i = 0; i < componentesEntrada.length; i++) {
                for (j = 0; j < etapas; j++) {
                    primerTerminoSuma[i][j] = primerTerminoPreSuma[i][j] * yNormalizado[i][j];
                    primerTerminoSumaLiquido[i][j] = primerTerminoPreSuma[i][j] * xNormalizado[i][j];
                }
            }
            let sumaFinalPrimerTermino = [];
            let sumaFinalPrimerTerminoLiquido = [];
            for (i = 0; i < etapas; i++) {
                suma = 0;
                sumaLiquido = 0;
                for (j = 0; j < componentesEntrada.length; j++) {
                    suma += primerTerminoSuma[j][i];
                    sumaLiquido += primerTerminoSumaLiquido[j][i];
                }
                sumaFinalPrimerTermino.push(suma);
                sumaFinalPrimerTerminoLiquido.push(sumaLiquido);
            }

            /* END Calcula primer termino  */
            /* START Segundo calculo  */
            let sumaFinalSegundoTermino = [];
            let sumaFinalSegundoTerminoLiquido = [];
            for (j = 0; j < etapas; j++) {
                aux = 0;
                auxLiquido = 0;
                aux = constanteDeGases * (Zv2[j] - 1 - (((3 * Math.pow(Aj2[j], 2)) / (2 * Bj2[j])) * (Math.log(1 + ((Bj2[j] * presionTorre) / Zv2[j])))));
                auxLiquido = constanteDeGases * (Zl2[j] - 1 - (((3 * Math.pow(Aj2[j], 2)) / (2 * Bj2[j])) * (Math.log(1 + ((Bj2[j] * presionTorre) / Zl2[j])))));

                sumaFinalSegundoTermino.push(aux);
                sumaFinalSegundoTerminoLiquido.push(auxLiquido);

            }
            /* END Segundo calculo  */
            /* START tercer Termino  */
            let sumaFinalTercerTermino = [];
            let sumaFinalTercerTerminoLiquido = [];
            for (j = 0; j < etapas; j++) {
                aux = 0;
                auxLiquido = 0;
                aux = (3 * Math.pow(Zv2[j], 2)) - (2 * Zv2[j]) + ((Bj2[j] * presionTorre) * ((Math.pow(Aj2[j], 2) / Bj2[j]) - (Bj2[j] * presionTorre) - 1));
                auxLiquido = (3 * Math.pow(Zl2[j], 2)) - (2 * Zl2[j]) + ((Bj2[j] * presionTorre) * ((Math.pow(Aj2[j], 2) / Bj2[j]) - (Bj2[j] * presionTorre) - 1));
                sumaFinalTercerTermino.push(aux);
                sumaFinalTercerTerminoLiquido.push(auxLiquido);
            }

            /*END tercer Termino */
            /* START cuarto Termino  */
            let sumaFinalCuartoTermino = [];
            let sumaFinalCuartoTerminoLiquido = [];
            for (j = 0; j < etapas; j++) {
                aux = 0;
                auxLiquido = 0;
                aux = (((Zv2[j] * presionTorre * Bj2[j]) / tj2[j]) * ((2 * (Math.pow(Aj2[j], 2) / Bj2[j])) - (2 * Bj2[j] * presionTorre) - 1)) - (((Math.pow(Aj2[j], 2) * Bj2[j] * presionTorre) / tj2[j]) * (presionTorre - 1));
                auxLiquido = (((Zl2[j] * presionTorre * Bj2[j]) / tj2[j]) * ((2 * (Math.pow(Aj2[j], 2) / Bj2[j])) - (2 * Bj2[j] * presionTorre) - 1)) - (((Math.pow(Aj2[j], 2) * Bj2[j] * presionTorre) / tj2[j]) * (presionTorre - 1));
                sumaFinalCuartoTermino.push(aux);
                sumaFinalCuartoTerminoLiquido.push(auxLiquido);
            }

            /* END cuarto termino  */

            /* START quinto Termino  */
            let sumaFinalQuintoTermino = [];
            let sumaFinalQuintoTerminoLiquido = [];
            for (j = 0; j < etapas; j++) {
                aux = (constanteDeGases * tj2[j]) * (3 / 2) * (-(Math.pow(Aj2[j], 2) / (Bj2[j] * tj2[j]))) * (1 + ((Bj2[j] * presionTorre) / Zv2[j]));
                auxLiquido = (constanteDeGases * tj2[j]) * (3 / 2) * (-(Math.pow(Aj2[j], 2) / (Bj2[j] * tj2[j]))) * (1 + ((Bj2[j] * presionTorre) / Zl2[j]));
                sumaFinalQuintoTermino.push(aux);
                sumaFinalQuintoTerminoLiquido.push(auxLiquido);
            }
            /* END quinto termino  */

            /* START sexto termino  */
            let sumaFinalSextoTermino = [];
            let sumaFinalSextoTerminoLiquido = [];
            for (j = 0; j < etapas; j++) {
                aux = (constanteDeGases * tj2[j] * ((3 * Math.pow(Aj2[j], 2)) / (2 * Bj2[j]))) * ((presionTorre / Zv2[j]) * (- (Bj2[j] / tj2[j])) - (((Bj2[j] * presionTorre) / (Math.pow(Zv2[j], 2))) * (sumaFinalCuartoTermino[j] / sumaFinalTercerTermino[j])));
                auxLiquido = (constanteDeGases * tj2[j] * ((3 * Math.pow(Aj2[j], 2)) / (2 * Bj2[j]))) * ((presionTorre / Zl2[j]) * (- (Bj2[j] / tj2[j])) - (((Bj2[j] * presionTorre) / (Math.pow(Zl2[j], 2))) * (sumaFinalCuartoTermino[j] / sumaFinalTercerTermino[j])));
                sumaFinalSextoTermino.push(aux);
                sumaFinalSextoTerminoLiquido.push(auxLiquido);
            }
            /* END sexto termino  */
            /* START suma de terminos */
            let derivadaVapor = [];
            let derivadaLiquida = [];
            for (j = 0; j < etapas; j++) {
                aux = sumaFinalPrimerTermino[j] + sumaFinalSegundoTermino[j] + (constanteDeGases * tj2[j] * (sumaFinalCuartoTermino[j] / sumaFinalTercerTermino[j])) - sumaFinalQuintoTermino[j] - sumaFinalSextoTermino[j];
                auxLiquido = sumaFinalPrimerTerminoLiquido[j] + sumaFinalSegundoTerminoLiquido[j] + (constanteDeGases * tj2[j] * (sumaFinalCuartoTerminoLiquido[j] / sumaFinalTercerTerminoLiquido[j])) - sumaFinalQuintoTerminoLiquido[j] - sumaFinalSextoTerminoLiquido[j];
                derivadaVapor.push(aux);
                derivadaLiquida.push(auxLiquido);
            }
            console.log("Derivada de vapor: ");
            console.log(derivadaVapor);
            console.log("Derivada liquido: ");
            console.log(derivadaLiquida);
            /* END suma de terminos */
            /* END  integral parcial  */

            let raphsonAj = [];
            let raphsonBj = [];
            let raphsonCj = [];
            let raphsonDj = [];
            for (j = 0; j < etapas; j++) {
                if (j == 0) {
                    raphsonAj[j] = 0;
                    raphsonCj[j] = vj2[j + 1] * derivadaVapor[j + 1];
                    raphsonDj[j] = -((vj2[j + 1] * hjv[j + 1]) + (flujoLiquido * entalpiahFl) - (lj2[j] * hjl[j]) - (vj2[j] * hjv[j]) - qj[j]);
                } else if (j == (etapas - 1)) {
                    raphsonAj[j] = lj2[j - 1] * derivadaLiquida[j - 1];
                    raphsonCj[j] = 0;

                    raphsonDj[j] = -((lj2[j - 1] * hjl[j - 1]) + (flujoGas * entalpiahFv) - (vj2[j] * hjv[j]) - (lj2[j] * hjl[j]) - qj[j]);
                } else {
                    raphsonAj[j] = lj2[j - 1] * derivadaLiquida[j - 1];
                    raphsonCj[j] = vj2[j + 1] * derivadaVapor[j + 1];
                    raphsonDj[j] = -((lj2[j - 1] * hjl[j - 1]) + (vj2[j + 1] * hjv[j + 1]) - (lj2[j] * hjl[j]) - (vj2[j] * hjv[j]) - qj[j]);
                }
                raphsonBj[j] = -((lj2[j] * (derivadaLiquida[j])) + (vj2[j] * derivadaVapor[j]));
            }
            deltaTjRaphson = thomas(raphsonAj, raphsonBj, raphsonCj, raphsonDj, etapas);
            console.log("Delta Raphson: ");
            console.log(deltaTjRaphson);
            for (j = 0; j < etapas; j++) {
                tj2[j] = deltaTjRaphson[j] + tj2[j];
            }
            console.log("Ultimas tj calculadas: ");
            console.log(JSON.stringify(tj2));
            tao = 0;

            for (j = 0; j < etapas; j++) {
                tao += Math.pow(deltaTjRaphson[j], 2);
            }
            console.log("Ciclo: " + auxiliarContador + " Valor de tao: " + tao);


            auxiliarContador++;
        } while (tao >= (0.01 * etapas));

        imprimirResultados(componentesEntrada, etapas, xNormalizado, kij, tj2, yNormalizado, vj2, lj2);
        //$('.modal').modal('hide');
        var end = Date.now();
        console.log("Tiempo de ejecucion: " + (end - start));




        function createMatrix() {
            var matriz = new Array(componentesEntrada.length);
            for (var i = 0; i < matriz.length; i++) {
                matriz[i] = new Array(parseInt(etapas));
            }
            return matriz;
        }

    } else {
        toastr.error("Verifique que la suma fraccionaria sea igual a 1", "Error");
    }// Termina ELSE de verificacion

}
function thomas(a, b, c, x, ene) {
    c[0] = c[0] / b[0];
    x[0] = x[0] / b[0];
    for (n = 1; n < ene; n++) {
        var m = 1 / (b[n] - a[n] * c[n - 1]);
        if (n < (ene - 1)) {
            c[n] = c[n] * m;
        }
        x[n] = (x[n] - a[n] * x[n - 1]) * m;
    }
    for (n = ene - 2; n >= 0; n--) {
        x[n] = x[n] - c[n] * x[n + 1];
    }
    return x;
}


